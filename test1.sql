SELECT
  a.*,
  coalesce((SELECT false FROM credit c WHERE c.client_id = a.client_id), true) AS new_client
FROM application a;

