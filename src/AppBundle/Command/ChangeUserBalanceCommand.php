<?php

namespace AppBundle\Command;

use Doctrine\DBAL\Connection;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ChangeUserBalanceCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:change-user-balance')
            ->setDescription('Move balance from user name to user name')
            ->setHelp('Move balance...')
            ->setDefinition(
                new InputDefinition([
                    new InputOption('from', 'f', InputOption::VALUE_REQUIRED),
                    new InputOption('to', 't', InputOption::VALUE_REQUIRED),
                    new InputOption('sum', 's', InputOption::VALUE_REQUIRED)
                ])
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Connection $connection */
        $connection = $this->getContainer()->get('doctrine')->getConnection();

        $sql = <<<SQL
SELECT 
  balance
FROM "user"
WHERE name = :name
SQL;

        $balanceFrom = $connection->fetchColumn($sql, ['name' => $input->getOption('from')]);

        if (0 == $balanceFrom || $input->getOption('sum') > $balanceFrom) {
            $output->writeln("Невозможно провести перевод баланса. На счету недостаточно средств!");

            return false;
        }

        $connection->beginTransaction();

        try {

            $sql = <<<SQL
WITH update_to AS (
    UPDATE "user"
    SET balance = (balance + :sum)
    WHERE NAME = :to
  )
UPDATE "user"
SET balance = (balance - :sum)
WHERE name = :from
SQL;

            $connection->executeUpdate($sql, [
                'sum' => $input->getOption('sum'),
                'from' => $input->getOption('from'),
                'to' => $input->getOption('to')
            ]);

            $connection->commit();
        } catch (Exception $e) {
            $connection->rollBack();
        }
    }
}