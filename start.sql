create table "user"
(
	id bigserial not null
		constraint user_pkey
			primary key,
	name varchar(255) not null,
	balance integer default 0 not null
)
;

create unique index user_id_uindex
	on "user" (id)
;

insert into "user" (name, balance) VALUES ('test1', 1000), ('test2', 0)
;
